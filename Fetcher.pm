package Fetcher;

use strict;
use warnings;

use Downloader::Download;
use JSON qw(decode_json);
use Method::Signatures;

method new {
       my $obj = bless {}, $self;
       $obj->{jsons} = [
			'http://media1.clubpenguin.com/play/en/web_service/game_configs/paper_items.json',
			'http://media1.clubpenguin.com/play/en/web_service/game_configs/igloos.json',
			'http://media1.clubpenguin.com/play/en/web_service/game_configs/furniture_items.json'
       ];  
       $obj->{methods} = {
               paper_items => 'downloadItems',
               igloos => 'downloadIgloos',
               furniture_items => 'downloadFurns'
       };
       return $obj;
}

method downloadEverything {
		my $resDownloader = Download->new;
		my $arrInfo = $resDownloader->asyncGetContent(\@{$self->{jsons}});
		while (my ($strKey, $arrData) = each(%{$arrInfo})) {
				if (exists($self->{methods}->{$strKey})) {
					my $strMethod = $self->{methods}->{$strKey};
					if ($self->can($strMethod)) {
						$self->$strMethod($resDownloader, decode_json($arrData));
					}
				}
		}
}

method downloadItems($resDownloader, $arrItems) {
		my @arrURLS = ();
		foreach (sort @{$arrItems}) {
				my $intItem = $_->{paper_item_id};
				my $strLink = "http://media8.clubpenguin.com/game/items/images/paper/icon/120/" . $intItem . ".png";
				push(@arrURLS, $strLink);
		}
		$resDownloader->asyncDownload('items/icons/', \@arrURLS);
}

method downloadIgloos($resDownloader, $arrIgloos) {
		my @arrURLS = ();
		foreach (sort keys %{$arrIgloos}) {
				my $intIgloo = $arrIgloos->{$_}->{igloo_id};
				my $strLink = "http://media8.clubpenguin.com/game/items/images/igloos/buildings/icon/120/" . $intIgloo . ".png";
				push(@arrURLS, $strLink);
		}
		$resDownloader->asyncDownload('igloos/icons/', \@arrURLS);
}

method downloadFurns($resDownloader, $arrFurns) {
		my @arrURLS = ();
		foreach (sort @{$arrFurns}) {
				my $intFurn = $_->{furniture_item_id};
				my $strLink = "http://media8.clubpenguin.com/game/items/images/furniture/icon/120/" . $intFurn . ".png";
				push(@arrURLS, $strLink);
		}
		$resDownloader->asyncDownload('furnitures/icons/', \@arrURLS);
}

1;

Fetcher->new->downloadEverything;
